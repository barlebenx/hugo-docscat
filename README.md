# hugo-docscat

Simple theme with category on the home page.

![Stip screenshot](https://raw.githubusercontent.com/nonovalo/hugo-docscat/master/images/screenshot.png)

This site is inspired from :

- [Learn](https://themes.gohugo.io/hugo-theme-learn/) hugo theme for the search.
- [Linode](https://github.com/linode/docs) hugo theme for presentation of the home page.



## Installation

### 1. Install the theme

```
git submodule add https://github.com/nonovalo/hugo-docscat.git themes/hugo-docscat
```

### 2. Configure Hugo

Add the following line to `config.toml` to tell Hugo to use the theme.

```
theme = "hugo-docscat"
```

## License

See [LICENSE](https://github.com/nonovalo/hugo-docscat/blob/master/LICENSE).

---
title: "Highlight"
date: 2018-10-26T12:59:38+02:00
draft: false
show_on_frontpage: false
description: Highlight code, usage example
---

[hugo documentation](https://gohugo.io/content-management/syntax-highlighting/)


## config.toml configuration

To enable highlighting by defaut in code block with language defined, add the following line in config.toml.

```toml
pygmentsCodeFences = true
```


## Example with python

````md
```python
print("my example !")
```
````

```python
print("my example !")
```


## Example with go-html-template

````md
```go-html-template
<section id="main">
  <div>
    <h1 id="title">{{ .Title }}</h1>
    {{ range .Pages }}
      {{ .Render "summary"}}
    {{ end }}
  </div>
</section>
```
````

```go-html-template
<section id="main">
  <div>
    <h1 id="title">{{ .Title }}</h1>
    {{ range .Pages }}
      {{ .Render "summary"}}
    {{ end }}
  </div>
</section>
```
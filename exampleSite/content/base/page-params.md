---
title: "Page params"
date: 2018-10-26T12:59:38+02:00
draft: false
show_on_frontpage: false
#icon:
description: Parameters list for page
---

## Default

- ``title`` : title of the page
- ``date`` : creation date of the page
- ``draft`` : draft or not

## Params for the home page

- ``show_on_frontpage`` : if we show this page on the home page
- ``icon`` : the icon on the home page for this page (icon [fontawesome](https://fontawesome.com/))

## Other

- ``description`` : short description of the page

